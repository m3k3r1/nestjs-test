import { Injectable, NotFoundException } from '@nestjs/common';
import {Task, TaskStatus} from './tasks.model';
import { v4 as uuidv4 } from 'uuid';
import {CreateTaskDTO} from './dto/create-task.dto';
import {GetTasksFilterDTO} from './dto/get-tasks-filter.dto';

@Injectable()
export class TasksService {
  private tasks: Task[] = [];

  getAllTasks(): Task[] {
    return this.tasks;
  }

  getTasksWithFilter(filterDto: GetTasksFilterDTO): Task[] {
    const {status, search} = filterDto;
    let tasks = this.getAllTasks();
    
    if(status) {
      tasks = tasks.filter(task => { task.status === status });
    }

    if(search) {
      tasks = tasks.filter(task => 
        task.title.includes(search) ||
        task.description.includes(search)
      );
    }

    return tasks;
  }

  getTaskById(id: string): Task {
    const taskFound = this.tasks.find(task => task.id === id );

    if(!taskFound){
      throw new NotFoundException(`Task with id ${id} not found`);
    }

    return taskFound;
  }
  
  createTask(createTaskDto: CreateTaskDTO): Task {
    const {title, description} = createTaskDto;

    const task: Task = {
      id: uuidv4(),
      title,
      description,
      status: TaskStatus.OPEN
    }

    this.tasks.push(task);
    return task;
  }

  deleteTask(id: string): void {
    const taskToBeDeleted = this.getTaskById(id);
    this.tasks = this.tasks.filter(task => task.id !== taskToBeDeleted.id);
  }

  updateTaskStatus(id: string, status: TaskStatus): Task {
    const task = this.getTaskById(id);
    task.status = status;
    return task;
  }
}
